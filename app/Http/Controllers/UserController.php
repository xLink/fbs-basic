<?php

namespace app\Http\Controllers;

use Faker\Factory;
use Illuminate\Routing\Controller as BaseController;

class UserController extends BaseController
{

    public function getUser($user_id)
    {
        $faker = Factory::create();
        $faker->seed($user_id);

        return view('profile')->withUser($faker);
    }
}
