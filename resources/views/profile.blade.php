<div class="page-header">
    <h1>
        <div class="page-title">{{ $user->name }} ({{ $user->username }})</div>
    </h1>
</div>

<div class="row">
    <div class="col-md-12">
        <ul>
            <li>{{ $user->email }}</li>
            <li>{{ $user->phonenumber }}</li>
            <li>{{ $user->country }}</li>
        </ul>
    </div>
</div>
